/*  Pipe connection Win64 testing (server)

    General
    -------
    Created on: 14/05/2018    
    @copyright: Oleh Kravets
    
    Contribute
    -------
    Source Code: https://gitlab.com/nightsky1/pipe_connection_test/blob/master/server.cpp

    Description on demand - kravecoleg@gmail.com
*/

#include <iostream>
#include <Windows.h>
#include <string>
#include <iomanip>
#define pipename "\\\\.\\pipe\\Pipe" // name of the future pipe declared


using namespace std;
  
  //data structure declaration
  struct Results
  {
    string date;
    int ideal;
    int real;
  }; 

  //template function for outputing tables
  template<typename T> void printElement(T t, int width)
  {
    cout << left << setw(width) << setfill(' ') << t ;
  }

int main(void)
{
  //Pipe-needed variables
  HANDLE hPipe;
  DWORD dwWrite;
  char buffer[1024]; // buffer for writing into the pipe
  int size; // for storing lenght of output string
  const void * a; // for transforming std::string value into const chat* value

  const int num = 3; // number of elements in the structure array 
  string new_line = "\n";
  string temp; // for storing the output string

  Results * Ltd1 = new Results[num]; // dynamic declaration of the array

  //STATIC DECLARATION OF THE DATA
  Ltd1[0].date = "September";
  Ltd1[1].date = "October";
  Ltd1[2].date = "November";

  Ltd1[0].ideal = 12;
  Ltd1[1].ideal = 45;
  Ltd1[2].ideal = 33;

  Ltd1[0].real = 2;
  Ltd1[1].real = 44;
  Ltd1[2].real = 36;


  //TABLE OUTPUT
  //header output
  printElement("Month:",17);
  printElement("Ideal outcome:",17);
  printElement("Real outcome:",17);
  cout << endl << endl;

  //info output
  for (int i = 0; i < num; ++i)
  {
    printElement(Ltd1[i].date, 17);
    printElement(Ltd1[i].ideal, 17);
    printElement(Ltd1[i].real, 17);
    cout << endl;
  }

  //GETTING THE NEEDED INFO
  for (int i = 0; i < num; i++)
  {
    if (Ltd1[i].real < Ltd1[i].ideal) // if the outcome is lover that ideal outcome
    {
      temp.append(Ltd1[i].date); // push the name of the month into the resulting string
      temp.append(new_line); // next line
    }    
  }

  //PIPE DECLARATION
  //declaration
  hPipe = CreateNamedPipe(TEXT(pipename),
                          PIPE_ACCESS_DUPLEX,
                          PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,   // FILE_FLAG_FIRST_PIPE_INSTANCE is not needed but forces CreateNamedPipe(..) to fail if the pipe already exists...
                          1,
                          1024 * 16,
                          1024 * 16,
                          NMPWAIT_USE_DEFAULT_WAIT,
                          NULL);
  //PIPE USAGE
  //while the pipe is true 
  while (hPipe != INVALID_HANDLE_VALUE)
  {
      if (ConnectNamedPipe(hPipe, NULL) != FALSE)   // wait for someone to connect to the pipe
      {
        for (int i = 0; i < num; i++)
        {
          size = temp.length() + 1;// size + place for '/0' to output correctly
          a = temp.c_str(); // turn std::string into const char*
          //Write the info into the pipe
          WriteFile(hPipe,
                a,
                size,   // = length of string + terminating '/0' 
                &dwWrite,
                NULL);
        }
        
      }
    // die();
    DisconnectNamedPipe(hPipe);
  }
  
  return 0;
}
