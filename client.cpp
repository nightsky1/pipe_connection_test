/*  Pipe connection Win64 testing (client)

    General
    -------
    Created on: 14/05/2018    
    @copyright: Oleh Kravets
    
    Contribute
    -------
    Source Code: https://gitlab.com/nightsky1/pipe_connection_test/blob/master/client.cpp

    Description on demand - kravecoleg@gmail.com
*/

#include <iostream>
#include <Windows.h>
#define pipename "\\\\.\\pipe\\Pipe"

using namespace std;

int main(void)
{
  //Pipe-needed variables
  HANDLE hPipe;
  DWORD dwRead;
  char buffer[1024]; //buffer for storing the output

  //OPENING THE PIPE
  hPipe = CreateFile(TEXT(pipename), 
                   GENERIC_READ | GENERIC_WRITE, 
                   0,
                   NULL,
                   OPEN_EXISTING,
                   0,
                   NULL);

  //READING FROM PIPE
  //while the pipe is true 
  if (hPipe != INVALID_HANDLE_VALUE)
  {
    ReadFile(hPipe, buffer, sizeof(buffer) - 1, &dwRead, NULL); //read from pipe into buffer 
                                                                //subtracting 1 byte for '/0'
    buffer[dwRead] = '\0';//adding the termination byte
    CloseHandle(hPipe);//closing the pipe

    cout << "These months were unprofitable for the company:" << endl << endl;

    printf("%s", buffer);//printing out the output
  }

  return (0);
}
